from Services.BankAccountService import BankAccountService

class ATM:
	
	def __init__( self ):
		print("ATM Starting")
		self.bank_account_service = BankAccountService()

	def withdraw_cash(self, amount):
		print("Withdraw 10")
		account_number = input("Enter Account Number: \r\n")
		self.bank_account_service.withdraw_funds(amount, account_number)

	def view_balance(self):
		print("View Balance")
		account_number = input("Enter Account Number: \r\n")
		print(self.bank_account_service.view_current_balance(account_number))
