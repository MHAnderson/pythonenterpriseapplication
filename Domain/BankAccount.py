from random import randint

class BankAccount:

	def __init__( self ):
		self.current_balance = 0
		self.account_number = randint(1, 9)

	def __init__( self, amount):
		self.current_balance = amount
		self.account_number = randint(1, 9)

	def view_current_balance(self):
		print(self.current_balance)

	def withdraw_cash(self, amount):
		self.current_balance -= amount

	def deposit_cash(self, amount):
		self.current_balance += amount

	def get_current_balance(self):
		return self.current_balance