from Domain.BankAccount import BankAccount
from DAO.BankAccountDAO import BankAccountDAO

class BankAccountService:
    
	def __init__( self ):
		self.bank_account_dao = BankAccountDAO()

	def withdraw_funds(self, amount, account_number):
		if self.bank_account_dao.IsAccountActive(account_number) == True:
			account = self.bank_account_dao.GetAccount(account_number)
			account.withdraw_cash(amount)
			self.bank_account_dao.UpdateAccount(account_number, account.get_current_balance)
		else:
			new_account = BankAccount(0)
			new_account.withdraw_cash(amount)
			self.bank_account_dao.UpdateAccount(account_number, new_account.get_current_balance)

		return "Transaction successful"

	def view_current_balance(self, account_number):
		if self.bank_account_dao.IsAccountActive(account_number) == True:
			account = self.bank_account_dao.GetAccount(account_number)
			return account.get_current_balance()
		else:
			return -11111

	